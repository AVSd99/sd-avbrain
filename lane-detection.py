# -*- coding: utf-8 -*-

import numpy as np
import cv2
from keras.models import load_model
import os

model = load_model('/content/drive/MyDrive/weights/Autopilot_V2.h5')


# model predicts a rotation
def keras_predict(model, image):
    processed = keras_process_image(image)
    steering_angle = float(model.predict(processed, batch_size=1))
    steering_angle = steering_angle * 100
    return steering_angle


# pre-processing part
def keras_process_image(img):
    image_x = 100
    image_y = 100
    img = cv2.resize(img, (image_x, image_y))
    img = np.array(img, dtype=np.float32)
    img = np.reshape(img, (-1, image_x, image_y, 1))
    return img


# drawing steering image in a frame
def steering_image(frame):
  global smoothed_angle

  gray = cv2.resize((cv2.cvtColor(frame, cv2.COLOR_RGB2HSV))[:, :, 1], (40, 40))
  steering_angle = keras_predict(model, gray)
  smoothed_angle += 0.2 * pow(abs((steering_angle - smoothed_angle)), 2.0 / 3.0) * (
      steering_angle - smoothed_angle) / abs(
      steering_angle - smoothed_angle)
  M = cv2.getRotationMatrix2D((cols / 2, rows / 2), -smoothed_angle, 1)
  dst = cv2.warpAffine(steer, M, (cols, rows))
  dst = cv2.resize(dst,(200,200))
  dst_x,dst_y = dst.shape[:2]
  frame_copy = frame.copy()
  height,width = frame_copy.shape[:2]
  x1,y1 = (width//2-dst_x//2,height-dst_y)
  x2,y2 = (x1+dst_x,height)
  frame_copy[y1:y2,x1:x2] = dst
  return cv2.resize(frame_copy,(width,height))



# draws a horizontal line on a frame
def horizontal(img):
    def pts_rectangle(pts, theta_):
    	dict_relation = {th_: pt for pt, th_ in zip(pts, theta_)}
    	avg = lambda x1: sum(x1) / len(x1) if len(x1) != 0 else 0
    	th = round(avg(theta_))
    	counter = 0
    	while True:
        	counter += 1
        	for coefficient in [0, -1, 1]:
	            try:
        	        return dict_relation[th + counter * coefficient]
	            except KeyError:
        	        pass


    img2 = img.copy()
    h, w = img2.shape[:2]
    p1 = (round(.33 * w), round(.71 * h))
    p2 = (round(.98 * w), round(.92 * h))
    img = img[p1[1]:p2[1], p1[0]:p2[0]]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.imwrite('gray.jpg',gray)
    edges = cv2.Canny(gray, 300, 10)
    cv2.imwrite('edges.jpg',edges)
    lines_ = []
    for theta in range(85, 96):
        lines_.append(cv2.HoughLinesP(edges, 1, theta * np.pi / 180, 125, None, 50, 50))
    pts = []
    theta_ = []
    for lines, theta in zip(lines_, range(85, 96)):
        try:
            for line in lines[0]:
                pt1 = (line[0], line[1])
                pt2 = (line[2], line[3])
                if pt1[0] == pt2[0]:
                    continue
                if abs((pt1[1] - pt2[1]) / (pt1[0] - pt2[0])) > .2:
                    continue
                pts.append([pt1, pt2])
                theta_.append(theta)
        except TypeError:
            pass

    if len(pts) >= 4:
        pt1, pt2 = pts_rectangle(pts, theta_)
        for u in range(-4, 5):
            m = (pt1[1] - pt2[1]) / (pt1[0] - pt2[0])
            b = pt1[1] - m * pt1[0]
            x0 = pt2[0] - round(.15 * (p2[0] - p1[0]))
            y0 = round(m * x0 + b)
            x3 = pt2[0] - round(.75 * (p2[0] - p1[0]))
            y3 = round(m * x3 + b)
            cv2.line(img, (x3, int(y3 + u)), (x0, int(y0 + u)), (0, 0, 255), 3)

    img2[p1[1]:p2[1], p1[0]:p2[0]] = img
    return img2


# combines horizontal and steering images togheter
def combine(img):
    return cv2.addWeighted(horizontal(img),.55,steering_image(img),.45,0)


steer = cv2.imread('/content/drive/MyDrive/weights/steering_wheel_image.jpg')
smoothed_angle = 0
rows, cols = steer.shape[:2]
vid = cv2.VideoCapture('/content/drive/MyDrive/vid/test.mp4')
vid2 = cv2.VideoCapture('/content/sd-avbrain/outputs/out1.avi')
frame_width, frame_height = 1280,720
out = cv2.VideoWriter('/content/sd-avbrain/outputs/out.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 30, (frame_width, frame_height))
while vid.isOpened():
    try:
      ret, frame = vid.read()
      _, frame2 = vid2.read()
      if ret:
        out.write(cv2.addWeighted(combine(frame),.4,frame2,.6,0))
      else:
        break
    except cv2.error:
      break
    
out.release()