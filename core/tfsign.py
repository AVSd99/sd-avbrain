from keras.models import load_model
import numpy as np
#import matplotlib.pyplot as plt
import cv2
#import pickle

import h5py
model1 = load_model('/content/drive/MyDrive/weights/model.h5')

#pickle_in=open("/content/drive/MyDrive/MyData/Example/model_trained.p","rb")  ## rb = READ BYTE
#model=pickle.load(pickle_in)

#fetch image
#import requests
#from PIL import Image
#url = 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Give_way_outdoor.jpg/400px-Give_way_outdoor.jpg'
#r = requests.get(url, stream=True)
#img = Image.open(r.raw)
# plt.imshow(img, cmap=plt.get_cmap('gray'))

#Open Image for testing
# 
# img = cv2.imread("5155701-german-traffic-sign-no-205.jpg",1)
# img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
# 
# plt.imshow(img, cmap=plt.get_cmap('gray'))

def getCalssName(classNo):
  if   classNo == 0: return 'Speed Limit 20 km/h'
  elif classNo == 1: return 'Speed Limit 30 km/h'
  elif classNo == 2: return 'Speed Limit 50 km/h'
  elif classNo == 3: return 'Speed Limit 60 km/h'
  elif classNo == 4: return 'Speed Limit 70 km/h'
  elif classNo == 5: return 'Speed Limit 80 km/h'
  elif classNo == 6: return 'Speed Limit 100 km/h'
  elif classNo == 7: return 'Speed Limit 120 km/h'
  elif classNo == 8: return 'Yield'
  elif classNo == 9: return 'No entry'
  elif classNo == 10: return 'Ahead only'
  elif classNo == 11: return 'No Left turn'
  elif classNo == 12: return 'No Right turn'
  elif classNo == 13: return 'No U turn'
  elif classNo == 14: return 'No Waiting'

def grayscale(img):
  img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  return img

def equalize(img):
  img = cv2.equalizeHist(img)
  return img

def preprocessing(img):
  img = grayscale(img)
  img = equalize(img)
  #normalize the images, i.e. convert the pixel values to fit btwn 0 and 1
  img = img/255
  return img

def Tfsign_class(img):

  #Preprocess image
  img = np.asarray(img)
  try:
    img = cv2.resize(img, (32, 32))
    img = preprocessing(img)
    img = img.reshape(1, 32, 32, 1)
    scores = model1.predict(img)
    return scores

  except cv2.error:
    return str('None')
    
  
  #img = preprocessing(img)
  #plt.imshow(img, cmap = plt.get_cmap('gray'))
  #print(img.shape)
  # Reshape reshape
  #image_h, image_w, _ = img.shape
  #if image_h==32 and image_w==32:
  #img = img.reshape(1, 32, 32, 1)
  #scores = model1.predict(img)
  #prediction = np.argmax(scores)
  
  #return str(prediction)
  
#Test image
#print("The Predicted sign is in Class: "+ str(model.predict_classes(img)))


