import numpy as np 
import cv2
import time
from keras.models import load_model


model = load_model('/content/drive/MyDrive/weights/model.h5')

# Trained weights can be found in the course mentioned above
path_to_weights = '/content/drive/MyDrive/weights/yolov4_custom_train_2000.weights'
path_to_cfg = '/content/drive/MyDrive/weights/yolov4_custom_test1.cfg'

# Loading trained YOLO v3 weights and cfg configuration file by 'dnn' library from OpenCV
network = cv2.dnn.readNetFromDarknet(path_to_cfg, path_to_weights)

# To use with GPU
#network.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
#network.setPreferableTarget(cv2.dnn.DNN_TARGET_OPENCL_FP16)
network.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
network.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)

# Getting names of all YOLO v3 layers
layers_all = network.getLayerNames()

# Getting only detection YOLO v3 layers that are 82, 94 and 106
layers_names_output = [layers_all[i[0] - 1] for i in network.getUnconnectedOutLayers()]

# Minimum probability to eliminate weak detections
probability_minimum = 0.26

# Setting threshold to filtering weak bounding boxes by non-maximum suppression
threshold = 0.26

def grayscale(img):
  img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  return img

def equalize(img):
  img = cv2.equalizeHist(img)
  return img

def preprocessing(img):
  img = grayscale(img)
  img = equalize(img)
  #normalize the images, i.e. convert the pixel values to fit btwn 0 and 1
  img = img/255
  return img

def getCalssName(classNo):
  if   classNo == 0: return 'Speed Limit 20 km/h'
  elif classNo == 1: return 'Speed Limit 30 km/h'
  elif classNo == 2: return 'Speed Limit 50 km/h'
  elif classNo == 3: return 'Speed Limit 60 km/h'
  elif classNo == 4: return 'Speed Limit 70 km/h'
  elif classNo == 5: return 'Speed Limit 80 km/h'
  elif classNo == 6: return 'Speed Limit 100 km/h'
  elif classNo == 7: return 'Speed Limit 120 km/h'
  elif classNo == 8: return 'Yield'
  elif classNo == 9: return 'No entry'
  elif classNo == 10: return 'Ahead only'
  elif classNo == 11: return 'No Left turn'
  elif classNo == 12: return 'No Right turn'
  elif classNo == 13: return 'No U turn'
  elif classNo == 14: return 'No Waiting'

def Tfsign_class(img):
  #Preprocess image
  img = np.asarray(img)
  img = cv2.resize(img, (32, 32))
  img = preprocessing(img)
  #Reshape reshape
  img = img.reshape(1, 32, 32, 1)
  scores = model.predict(img)   
  return scores
 
# Variable for counting total processing time
t = 0

# Catching frames in the loop
def tfsign(frame):
   imgf = frame.copy()
   h, w = frame.shape[:2]
  
   # Blob from current frame
   blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (416, 416), swapRB=True, crop=False) 
   # Forward pass with blob through output layers
   network.setInput(blob)
   output_from_network = network.forward(layers_names_output)

   # Lists for detected bounding boxes, confidences and class's number
   bounding_boxes = []
   confidences = []
   class_numbers = []
   # Going through all output layers after feed forward pass
   for result in output_from_network:
       # Going through all detections from current output layer
       for detected_objects in result:
           # Getting classes probabilities for current detected object
           scores = detected_objects[5:]
           # Getting index of the class with the maximum value of probability
           class_current = np.argmax(scores)
           # Getting value of probability for defined class
           confidence_current = scores[class_current] 
           # Eliminating weak predictions by minimum probability
           if confidence_current > probability_minimum:
               # Scaling bounding box coordinates to the initial frame size
               box_current = detected_objects[0:4] * np.array([w, h, w, h]) 
               # Getting top left corner coordinates
               x_center, y_center, box_width, box_height = box_current
               x_min = int(x_center - (box_width / 2))
               y_min = int(y_center - (box_height / 2)) 
               # Adding results into prepared lists
               bounding_boxes.append([x_min, y_min, int(box_width), int(box_height)])
               confidences.append(float(confidence_current))
               class_numbers.append(class_current)
            
   # Implementing non-maximum suppression of given bounding boxes
   results = cv2.dnn.NMSBoxes(bounding_boxes, confidences, probability_minimum, threshold) 
   # Checking if there is any detected object been left
   if len(results) > 0:
       # Going through indexes of results
       for i in results.flatten():
           # Bounding box coordinates, its width and height
           x_min, y_min = bounding_boxes[i][0], bounding_boxes[i][1]
           box_width, box_height = bounding_boxes[i][2], bounding_boxes[i][3]
           
           
           # Cut fragment with Traffic Sign
           c_ts = imgf[y_min-5:y_min+int(box_height)+5, x_min-5:x_min+int(box_width)+5, :]
           # print(c_ts.shape)
           
           if c_ts.shape[:1] == (0,) or c_ts.shape[1:2] == (0,):
               pass
           else:
               scores = Tfsign_class(c_ts)
               if scores is not None:
                 prediction = np.argmax(scores)
                 probabilityValue = np.amax(scores)
                 probabilityValue = round(probabilityValue,2)
                 # Colour for current bounding box
                 colour_box_current = [0, 50, 255] 
                 if probabilityValue > 0.7:
                   print(getCalssName(prediction))
                   cv2.putText(imgf,str(probabilityValue)+","+str(getCalssName(prediction)),
                       (x_min, y_min - 15),cv2.FONT_HERSHEY_SIMPLEX, 0.9, colour_box_current, 2)
                   # Drawing bounding box on the original current frame
                   cv2.rectangle(imgf, (x_min, y_min),
                                 (x_min + box_width, y_min + box_height),
                                 colour_box_current, 2)
   return imgf              
