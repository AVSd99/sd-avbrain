import os
import cv2
import random
import numpy as np
import tensorflow as tf
from core.utils import read_class_names
from core.config import cfg
#from core.tfsign import Tfsign_class


def combined(boxes, pred_conf1):
  boxes1, scores1, classes1, valid_detections1 = tf.image.combined_non_max_suppression(
            boxes=tf.reshape(boxes, (tf.shape(boxes)[0], -1, 1, 4)),
            scores=tf.reshape(
                pred_conf1, (tf.shape(pred_conf1)[0], -1, tf.shape(pred_conf1)[-1])),
            max_output_size_per_class=50,
            max_total_size=50,
            iou_threshold=0.40,
            score_threshold=0.60
        )
  return boxes1, scores1, classes1, valid_detections1      



# function to count objects, can return total classes or count per class
def count_objects(data, by_class = False, allowed_classes = list(read_class_names(cfg.YOLO.CLASSES).values())):
    boxes, scores, classes, num_objects = data

    #create dictionary to hold count of objects
    counts = dict()
    # if by_class = True then count objects per class
    if by_class:
        class_names = read_class_names(cfg.YOLO.CLASSES)

        # loop through total number of objects found
        for i in range(num_objects):
            # grab class index and convert into corresponding class name
            class_index = int(classes[i])
            class_name = class_names[class_index]
            if class_name in allowed_classes:
                counts[class_name] = counts.get(class_name, 0) + 1
            else:
                continue

    # else count total objects found
    else:
        counts['total object'] = num_objects
    
    return counts

# function for cropping each detection and saving as new image
def crop_objects(img, data, allowed_classes):
    boxes, scores, classes, num_objects = data
    class_names = read_class_names(cfg.YOLO.CLASSES)
    #create dictionary to hold count of objects for image name
    counts = dict()
    for i in range(num_objects):
      # get count of class for part of image name
      class_index = int(classes[i])
      class_name = class_names[class_index]

      if class_name in allowed_classes:
        counts[class_name] = counts.get(class_name, 0) + 1
        # get box coords
        xmin, ymin, xmax, ymax = boxes[i]
        # crop detection from image (take an additional 5 pixels around all edges)
        cropped_img = img[int(ymin)-5:int(ymax)+5, int(xmin)-5:int(xmax)+5]
        
        #if cropped_img is None:                #If the image data is empty, skip
        #  continue 
        #else:  
        #  #cropped_img = cv2.resize(cropped_img, (32, 32))
        #  TfClass = Tfsign_class(cropped_img)
        #  print("TfClass >>>>>>>>>>>>>" + TfClass)
  
        #bbox_color = (0,0,0)
        #bbox_thick = int(0.6 * (image_h + image_w) / 600)            
        #cv2.rectangle(img, (xmin, ymin), (xmax, ymax), bbox_color, bbox_thick)
         
      else:
        continue
    #return img         
        
def dis_objects(img, data, allowed_classes):
    boxes, scores, classes, num_objects = data
    class_names = read_class_names(cfg.YOLO.CLASSES)
    img_h, img_w, _ = img.shape
    size_thick = int(0.6 * (img_h + img_w) / 600)
    #create dictionary to hold count of objects for image name
    counts = dict()
    for i in range(num_objects):
      # get count of class for part of image name
      class_index = int(classes[i])
      class_name = class_names[class_index]

      if class_name in allowed_classes:
        counts[class_name] = counts.get(class_name, 0) + 1
        # get box coords
        xmin, ymin, xmax, ymax = boxes[i]
        
        mid_x = int((xmin+xmax)/2)
        mid_y = int((ymin+ymax)/2)

        if mid_x > int(img_w*0.37) and mid_x < int(img_w*0.63) and ymin < int(img_h*0.50):
          h1 = ymax-ymin
          #w1 = xmax-xmin
          #s1 = h1*w1
          if class_name in ['bus','truck']: k1, k2 = 0.9, 0.4
          if class_name in ['car']: k1, k2 = 1, 0.5
          dis = round(((1-(k1*(h1/img_h) + k2*(ymax/img_h)))*11),1)   
          print('dis=',dis,'k1=',k1,'k2=',k2) 
          if dis > 6.4:
            continue
          else:                            
            cv2.putText(img,'dis= ' + str(dis) + 'm', (int(xmin+3), int(ymax-3)),cv2.FONT_HERSHEY_SIMPLEX, 1, (255,0,255), 2)
          if dis < 3.1:
            cv2.putText(img, 'Warning!', (xmin,mid_y), cv2.FONT_HERSHEY_SIMPLEX, 1.6, (255,0,255), 3)
         
      else:
        continue

    return img


