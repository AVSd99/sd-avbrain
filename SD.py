import os
# comment out below line to enable tensorflow logging outputs
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import time
import tensorflow as tf
physical_devices = tf.config.experimental.list_physical_devices('GPU')
if len(physical_devices) > 0:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)
from absl import app, flags, logging
from absl.flags import FLAGS
import core.utils as utils
from core.yolov4 import filter_boxes
from core.functions import *
from core.tfsign2 import tfsign
from tensorflow.python.saved_model import tag_constants
from core.config import cfg
from PIL import Image
import cv2
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

flags.DEFINE_string('framework', 'tf', '(tf, tflite, trt')
flags.DEFINE_string('weights', './checkpoints/yolov4-416',
                    'path to weights file')
flags.DEFINE_integer('size', 416, 'resize images to')
flags.DEFINE_boolean('tiny', False, 'yolo or yolo-tiny')
flags.DEFINE_string('model', 'yolov4', 'yolov3 or yolov4')
flags.DEFINE_string('video', './data/video/test.mp4', 'path to input video or set to 0 for webcam')
flags.DEFINE_string('output', None, 'path to output video')
flags.DEFINE_string('output_format', 'XVID', 'codec used in VideoWriter when saving video to file')
flags.DEFINE_float('iou', 0.45, 'iou threshold')
flags.DEFINE_float('score', 0.64, 'score threshold')
flags.DEFINE_boolean('dont_show', False, 'dont show video output')
flags.DEFINE_boolean('info', False, 'show detailed info of tracked objects')
flags.DEFINE_boolean('count', False, 'count objects being tracked on screen')
flags.DEFINE_boolean('crop', False, 'crop detections from images')
flags.DEFINE_boolean('dis', False, 'distance detections from images')

def main(_argv):

    # load configuration for object detector
    config = ConfigProto()
    config.gpu_options.allow_growth = True
    session = InteractiveSession(config=config)
    STRIDES, ANCHORS, NUM_CLASS, XYSCALE = utils.load_config(FLAGS)
    input_size = FLAGS.size
    video_path = FLAGS.video

    # load tflite model if flag is set
    if FLAGS.framework == 'tflite':
        interpreter = tf.lite.Interpreter(model_path=FLAGS.weights)
        interpreter.allocate_tensors()
        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()
        print(input_details)
        print(output_details)
    # otherwise load standard tensorflow saved model
    else:
        saved_model_loaded = tf.saved_model.load(FLAGS.weights, tags=[tag_constants.SERVING])
        infer = saved_model_loaded.signatures['serving_default']

    # begin video capture
    try:
        vid = cv2.VideoCapture(int(video_path))
    except:
        vid = cv2.VideoCapture(video_path)
#
#    out = None
#
#    # get video ready to save locally if flag is set
#    if FLAGS.output:
#        # by default VideoCapture returns float instead of int
#        width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
#        height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
#        fps = int(vid.get(cv2.CAP_PROP_FPS))
#        codec = cv2.VideoWriter_fourcc(*FLAGS.output_format)
#        out = cv2.VideoWriter(FLAGS.output, codec, fps, (width, height))
#################>>>>>
    data_frame = {}
    frame_num = 0
    # while video is running
    while True:
        return_value, frame = vid.read()
        if return_value:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            image = Image.fromarray(frame)
        else:
            print('Video has ended or failed, try a different video format!')
            break
        frame_num +=1
        print('Frame #: ', frame_num)
        frame_size = frame.shape[:2]
        image_data = cv2.resize(frame, (input_size, input_size))
        image_data = image_data / 255.
        image_data = image_data[np.newaxis, ...].astype(np.float32)
        start_time = time.time()

        # run detections on tflite if flag is set
        if FLAGS.framework == 'tflite':
            interpreter.set_tensor(input_details[0]['index'], image_data)
            interpreter.invoke()
            pred = [interpreter.get_tensor(output_details[i]['index']) for i in range(len(output_details))]
            # run detections using yolov3 if flag is set
            if FLAGS.model == 'yolov3' and FLAGS.tiny == True:
                boxes, pred_conf = filter_boxes(pred[1], pred[0], score_threshold=0.25,
                                                input_shape=tf.constant([input_size, input_size]))
            else:
                boxes, pred_conf = filter_boxes(pred[0], pred[1], score_threshold=0.25,
                                                input_shape=tf.constant([input_size, input_size]))
        else:
            batch_data = tf.constant(image_data)
            pred_bbox = infer(batch_data)
            for key, value in pred_bbox.items():
                boxes = value[:, :, 0:4]
                pred_conf = value[:, :, 4:]

        boxes, scores, classes, valid_detections = tf.image.combined_non_max_suppression(
            boxes=tf.reshape(boxes, (tf.shape(boxes)[0], -1, 1, 4)),
            scores=tf.reshape(
                pred_conf, (tf.shape(pred_conf)[0], -1, tf.shape(pred_conf)[-1])),
            max_output_size_per_class=50,
            max_total_size=50,
            iou_threshold=FLAGS.iou,
            score_threshold=FLAGS.score
        )

        # convert data to numpy arrays and slice out unused elements
        num_objects = valid_detections.numpy()[0]
        bboxes = boxes.numpy()[0]
        bboxes = bboxes[0:int(num_objects)]
        scores = scores.numpy()[0]
        scores = scores[0:int(num_objects)]
        classes = classes.numpy()[0]
        classes = classes[0:int(num_objects)]

        # format bounding boxes from normalized ymin, xmin, ymax, xmax ---> xmin, ymin, width, height
        original_h, original_w, _ = frame.shape
        bboxes = utils.format_boxes(bboxes, original_h, original_w)

        # store all predictions in one parameter for simplicity when calling functions
        pred_bbox = [bboxes, scores, classes, num_objects]
        data_frame[frame_num] = pred_bbox
###### >>>>>>>>>>>>>>>>>>>>>        
        # read in all class names from config
        class_names = utils.read_class_names(cfg.YOLO.CLASSES)
        
        # by default allow all classes in .names file
        #allowed_classes = list(class_names.values())

        # custom allowed classes (uncomment line below to customize tracker for only people)
        allowed_classes1 = ['traffic light']
        allowed_classes = ['person','car','bicycle','motorbike','bus','truck','traffic light']

#######
#        if FLAGS.count:
#            # count objects found
#            counted_classes = count_objects(pred_bbox, by_class = True, allowed_classes=allowed_classes)
#            # loop through dict and print
#            for key, value in counted_classes.items():
#                print("Number of {}s: {}".format(key, value))
#            image = utils.draw_bbox(frame, pred_bbox, FLAGS.info, counted_classes, allowed_classes=allowed_classes)
#        else:
#            image = utils.draw_bbox(frame, pred_bbox, FLAGS.info, allowed_classes=allowed_classes)
#######
	
	    # if crop flag is enabled, crop each detection and save it as new image
#        if FLAGS.crop:
#            crop_rate = 1 # capture images every so many frames (ex. crop photos every 150 frames)
#            if frame_num % crop_rate == 0:
#                try:
#                  crop_objects(frame, pred_bbox, allowed_classes1)
#                except FileExistsError:
#                  pass                     
#            else:
#                pass
	
        # if dis flag is enabled, calculate each detection distance
#        if FLAGS.dis:
#            try:
#                image = dis_objects(frame, pred_bbox, ['car','bus','truck'])
#            except FileExistsError:
#                pass

        # calculate frames per second of running detections
        fps = 1.0 / (time.time() - start_time)
        print("FPS: %.2f" % fps)
        result = np.asarray(frame)
        result = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        
        if not FLAGS.dont_show:
            cv2.imshow("Output Video", result)
#        
#        # if output flag is set, save video file
#        if FLAGS.output:
#            out.write(result)
#        if cv2.waitKey(1) & 0xFF == ord('q'): break
#    cv2.destroyAllWindows()
################################### TFSign
    # Trained weights can be found in the course mentioned above
    print("running TF detect...")

    # begin video capture
    try:
        vid = cv2.VideoCapture(int(video_path))
    except:
        vid = cv2.VideoCapture(video_path)

    out = None

    # get video ready to save locally if flag is set
    if FLAGS.output:
        # by default VideoCapture returns float instead of int
        width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
        fps = int(vid.get(cv2.CAP_PROP_FPS))
        codec = cv2.VideoWriter_fourcc(*FLAGS.output_format)
        out = cv2.VideoWriter(FLAGS.output, codec, fps, (width, height))
      
    # Variable for counting total processing time
    t = 0
    
    frame_num = 0
    while True:
      return_value, frame = vid.read()
       
      if not return_value:
          print('Video has ended or failed, try a different video format!')
          print('Total number of frames', frame_num)
          print('Total amount of time {:.5f} seconds'.format(t))
          break
      frame_num +=1
      print('Frame #: ', frame_num)
      data1 = data_frame[frame_num]
   
#      allowed_classes1 = ['traffic light']
      allowed_classes = ['person','car','bicycle','motorbike','bus','truck','traffic light']  
 
      start = time.time()
      end = time.time()

      # Increasing counters
      t += end - start

      frame = tfsign(frame)

      
      if FLAGS.count:
            # count objects found
            counted_classes = count_objects(data1, by_class = True, allowed_classes=allowed_classes)
            # loop through dict and print
            for key, value in counted_classes.items():
                print("Number of {}s: {}".format(key, value))
            frame = utils.draw_bbox(frame, data1, FLAGS.info, counted_classes, allowed_classes=allowed_classes)
      else:
            frame = utils.draw_bbox(frame, data1, FLAGS.info, allowed_classes=allowed_classes)

      if FLAGS.dis:
          try:
              frame = dis_objects(frame, data1, ['car','bus','truck'])
          except FileExistsError:
              pass
      
      # calculate frames per second of running detections
      fps = 1.0 / (time.time() - start_time)
      print("FPS: %.2f" % fps)
      result = np.asarray(frame)
#      result = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
      
      if not FLAGS.dont_show:
          cv2.imshow("Output Video", result)      
      # if output flag is set, save video file
      if FLAGS.output:
          out.write(result)
#      if cv2.waitKey(1) & 0xFF == ord('q'): break
#      cv2.destroyAllWindows()
    
      
      
      
if __name__ == '__main__':
    try:
        app.run(main)
    except SystemExit:
        pass
